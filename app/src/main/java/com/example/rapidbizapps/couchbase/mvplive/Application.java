package com.example.rapidbizapps.couchbase.mvplive;

/**
 * Application class for the project.
 */
public final class Application extends android.app.Application {
    private static final String TAG = Application.class.getSimpleName();

    private static Application sInstance;

    /**
     * Provides a static instance of the {@link Application} class.
     * </br>
     * HINT: The instance can be used to access the application context, etc.
     *
     * @return Static instance of the Application class.
     */
    public static Application getInstance() {
        return sInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
    }
}
