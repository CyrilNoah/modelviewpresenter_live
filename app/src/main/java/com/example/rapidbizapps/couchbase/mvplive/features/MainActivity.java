package com.example.rapidbizapps.couchbase.mvplive.features;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.rapidbizapps.couchbase.mvplive.R;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
